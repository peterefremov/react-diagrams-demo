import * as path from 'path';
import * as webpack from 'webpack';
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

interface EnvironmentParams {
	production?: boolean;
}

function srcPath(subdir: string) {
	return path.join(__dirname, "src", subdir);
}

const ENTRYPOINT = "src/index.tsx";
const BUILD_DIR = "dist/";

const TSX_RULE_DEV = {
	test: /\.tsx?$/,
	include: path.resolve(__dirname, 'src'),
	exclude: /node_modules/,
	use: [
		"ts-loader"
	]
};

const TSX_RULE_PRODUCT = {
	test: /\.tsx?$/,
	include: path.resolve(__dirname, 'src'),
	exclude: /node_modules/,
	use: [
		"ts-loader"
	]
};

const config: webpack.Configuration = {
	mode: 'development',
	entry: {
		bundle: path.join(__dirname, ENTRYPOINT),
	},
	output: {
		filename: "[name].js",
		path: path.join(__dirname, 'dist'),
		publicPath: "/"
	},

	optimization: {
		//runtimeChunk: 'single',
		splitChunks: {
			cacheGroups: {
				vendor: {
					chunks: "initial",
					test: path.resolve(process.cwd(), "node_modules"),
					name: "vendor",
					enforce: true
				}
			}
		},
		concatenateModules: true,
		usedExports: true,
		sideEffects: true
	},

	resolve: {
		extensions: [".ts", ".tsx", ".js", ".json"],
	},

	module: {
		rules: [
			{ enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
		]
	},

	plugins: [],

	// When importing a module whose path matches one of the following, just
	// assume a corresponding global variable exists and use that instead.
	// This is important because it allows us to avoid bundling all of our
	// dependencies, which allows browsers to cache those libraries between builds.
	/* externals: [
		{
			"react": "React",
			"react-dom": "ReactDOM",
		}
	] */
};

const getConfig = (env: EnvironmentParams) => {
	if(!env) env = {};
	let ENV = "dev";
	const {production = false} = env;
	if(!config.plugins) config.plugins = [];

	if(production) {
		ENV = "prod";
		config.mode = "production";

		config.output = {
			filename: "[name].js",
			path: path.join(__dirname, BUILD_DIR),
			publicPath: "/"
		};

		if(config.module && config.module.rules) {
			config.module.rules.push(TSX_RULE_PRODUCT);
		}

	} else {
		config.devServer = {
			contentBase: path.join(__dirname, "dist"),
			compress: true,
			port: 9000,
			hot: true,
			hotOnly: true,
			clientLogLevel: "warning",
			historyApiFallback: true
		};

		config.devtool = 'source-map';

		config.plugins.push(new webpack.NamedModulesPlugin());
		config.plugins.push(new webpack.HotModuleReplacementPlugin());
		//config.plugins.push(new BundleAnalyzerPlugin());

		if(config.module && config.module.rules) {
			config.module.rules.push(TSX_RULE_DEV);
		}
	}

	config.plugins.push(
		new webpack.DefinePlugin({
			ENV: JSON.stringify(ENV)
		})
	);

	return config;
};

export default getConfig;
