import * as React from 'react';
import Demo from './demo';

type Props = {
	
};

type State = {
	
};

export default class Application extends React.Component<Props, State> {

	public constructor(props: Props) {
		super(props);

		const state: State = {
			
		};

		this.state = state;
	}

	public render() {
		
		return (
			<>
				<h1 style={{margin: "10px auto"}}>Demo</h1>
				<br />
				<div style={{padding: "10px", fontSize: "20px"}}>
<b>Delete</b> removes any selected items
<br />
<b>Shift + Mouse Drag</b> triggers a multi-selection box
<br />
<b>Shift + Mouse Click</b> selects the item (items can be multi-selected)
<br />
<b>Mouse Drag drags</b> the entire diagram
<br />
<b>Mouse Wheel</b> zooms the diagram in / out
<br />
<b>Click Link + Drag</b> creates a new link point
<br />
{/* <b>Click Node Port + Drag</b> creates a new link */}
				</div>

				<Demo />
			</>
		);

	}

}
