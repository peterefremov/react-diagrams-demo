import { NodesList } from './index';

const NODES_DEMO: NodesList = {
	0: {
		id: 0,
		type: "start",
		position: {x: 50, y: 50},
		connections: [1],
	},
	1: {
		id: 1,
		type: "dialogue",
		position: {x: 150, y: 50},
		connections: [2],
	},
	2: {
		id: 2,
		type: "quiz",
		position: {x: 250, y: 50},
		connections: [3],
	},
	3: {
		id: 3,
		type: "transition",
		position: {x: 350, y: 50},
		connections: [4],
	},
	4: {
		id: 4,
		type: "add",
		position: {x: 450, y: 50},
		connections: [],
	}
};

export default NODES_DEMO;
