import * as React from 'react';
import createEngine, { DefaultLinkModel, DiagramModel, DefaultNodeModel } from '@projectstorm/react-diagrams';
import { BodyWidget } from './BodyWidget';
import { RPGNodeFactory, RPGNodeModel, RPGNodeType } from './RPGNode';
import NODES_DEMO from './DemoProject';

type Props = {
	
};

type State = {
	
};

export interface NodeItem {
	id: number;
	type: RPGNodeType;
	position: {x: number, y: number};
	connections: number[];
}

export interface NodesList {
	[key: number]: NodeItem;
}

export default class Demo extends React.Component<Props, State> {

	private engine = createEngine();
	private model = new DiagramModel();

	private nodesSpeck: NodesList = NODES_DEMO;
	private nodesRender: {[key: number]: RPGNodeModel} = {};

	public constructor(props: Props) {
		super(props);

		this.engine.getNodeFactories().registerFactory(new RPGNodeFactory({
			onAddNew: this.onAddNew
		}));

		const state: State = {
			
		};

		this.state = state;

		this.init();
	}

	/**
	 * Mock add function. Adds two new nodes. Fired once.
	 */
	private onAddNew = () => {
		const addAfterId = 4;
		const latestNode = this.nodesSpeck[addAfterId];
		if(!latestNode) return;
		latestNode.type = "dialogue";
		latestNode.connections.push(5);

		this.nodesSpeck[5] = {
			id: 5,
			type: "quiz",
			position: {
				x: latestNode.position.x + 100,
				y: latestNode.position.y,
			},
			connections: []
		};

		this.renderNodes();
		this.renderConnections();
		this.forceUpdate();
	}

	private renderNodes = () => {
		const addNodes: RPGNodeModel[] = [];

		for(const i in this.nodesSpeck) {
			const nodeSpeck = this.nodesSpeck[i];
			const {id, type, position, connections} = nodeSpeck;

			if(this.nodesRender[id]) {
				this.nodesRender[id].remove();
			}

			const node = new RPGNodeModel({
				speckID: id,
				type
			});
			
			node.setPosition(position.x, position.y);
			this.nodesRender[id] = node;
			addNodes.push(node);
		}

		this.model.addAll(...addNodes);

		//TODO: test that events do not attach multiple times.
		addNodes.forEach( item => {
			item.registerListener({
				eventWillFire: this.nodeEvent
			});
		});
	}

	private nodeEvent = (e: any) => {
		//TODO: find typings for react-diagrams events. Called BaseEvent in internal source files. Not sure it is importable.
		const entity = (e as any).entity;
		const {position, speckID} = entity;

		if(this.nodesSpeck[speckID]) {
			this.nodesSpeck[speckID].position = position;
		}
	}

	private modelEvent = (e: any) => {
		
	}

	private renderConnections = () => {
		const {nodesRender} = this;
		const addLinks: DefaultLinkModel[] = [];

		for(const i in this.nodesSpeck) {
			const nodeSpeck = this.nodesSpeck[i];
			const {id, type, position, connections} = nodeSpeck;

			const sourceNode = nodesRender[id];
			const targetNode = nodesRender[ connections[0] ];

			//TODO: throw error here
			if(!sourceNode || !targetNode) continue;

			const link = new DefaultLinkModel();

			const portOut = sourceNode.getPort('out');
			const portIn = targetNode.getPort('in');

			//TODO: throw error here
			if(!portOut || !portIn) continue;

			link.setSourcePort(portOut);
			link.setTargetPort(portIn);

			addLinks.push(link);
		}

		this.model.addAll(...addLinks);
	}

	private init = () => {
		this.renderNodes();
		this.renderConnections();

		this.model.registerListener({
			eventDidFire: this.modelEvent
		});

		this.loadEngine();
	}

	private loadEngine = () => {
		this.engine.setModel(this.model);
	}

	private onCanvasClick = (e: React.MouseEvent<HTMLDivElement>) => {
		
	}

	public render() {
		
		return (
			<div onClick={this.onCanvasClick} style={{flexGrow: 1, display: "flex", flexDirection: "column"}}>
				<BodyWidget engine={this.engine} />
			</div>
		);

	}

}
