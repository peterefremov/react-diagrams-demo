import styled from '@emotion/styled';
import { Color } from 'csstype';

export const RPGNodeContainer = styled.div<{color: Color}>`
	position: relative;
	cursor: pointer;
	border-radius: 20px;
	width: 40px;
	height: 40px;
	background-color: ${props => props.color || "white"};
`;
