import * as React from 'react';
import { DiagramEngine, PortWidget } from '@projectstorm/react-diagrams-core';
import { MdPlayArrow, MdQuestionAnswer, MdLiveHelp, MdTimeline, MdAdd } from "react-icons/md";
import RPGNodeModel from './NodeModel';

import {
	RPGNodeContainer
} from './style';

export interface NodeWidgetProps {
	node: RPGNodeModel;
	engine: DiagramEngine;
	onAddNew: () => void;
}

export interface NodeWidgetState {}

export default class NodeWidget extends React.Component<NodeWidgetProps, NodeWidgetState> {
	constructor(props: NodeWidgetProps) {
		super(props);
		this.state = {};
	}

	private renderIcon = () => {
		const {node} = this.props;
		const {type} = node;
		const size = 30;
		const color = "white";

		switch(type) {
			case "start":
				return <MdPlayArrow color={color} size={size}/>;

			case "dialogue":
				return <MdQuestionAnswer color={color} size={size}/>;

			case "quiz":
				return <MdLiveHelp color={color} size={size}/>;

			case "transition":
				return <MdTimeline color={color} size={size}/>;

			case "add":
				return <MdAdd color={color} size={size}/>;
		}

		return <MdPlayArrow color={color} size={size}/>;
	}

	private getColor = (): string => {
		const {node} = this.props;
		const {type} = node;

		switch(type) {
			case "start": return "#494949";
			case "dialogue": return "#00a0ff";
			case "quiz": return "#ff9a00";
			case "transition": return "#bd37ff";
			case "add": return "#d0d0d0";
		}

		return "white";
	}

	public render() {
		const {node, onAddNew} = this.props;
		const {type} = node;
		const backgroundColor = this.getColor();

		console.log(node);

		return (
			<RPGNodeContainer onClick={type === "add" ? onAddNew : undefined} color={backgroundColor} className="rpg-node">

				<div style={{position: "absolute", top: "6px", left: "6px"}}>
					<this.renderIcon />
				</div>

				<PortWidget style={{marginTop: "10px"}} engine={this.props.engine} port={this.props.node.getPort('in') as any}>
					<div className="circle-port" />
				</PortWidget>
				<PortWidget engine={this.props.engine} port={this.props.node.getPort('out') as any}>
					<div className="circle-port" />
				</PortWidget>
				<div className="custom-node-color" style={{ backgroundColor: this.props.node.color }} />
			</RPGNodeContainer>
		);
	}
}
