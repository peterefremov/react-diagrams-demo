import { NodeModel, DefaultPortModel, DefaultPortModelOptions } from '@projectstorm/react-diagrams';
import { BaseModelOptions } from '@projectstorm/react-canvas-core';
import { RPGNodeType } from './type';

export interface RPGNodeModelOptions extends BaseModelOptions {
	color?: string;
	type: RPGNodeType;
	speckID: number;
}

export default class CustomNodeModel extends NodeModel {
	public color: string;
	public type: RPGNodeType;
	public speckID: number;

	constructor(options: RPGNodeModelOptions = {
		type: "start",
		speckID: 0,
	}) {
		super({
			...options,
			type: 'rpgnode'
		});
		this.color = options.color || 'magenta';
		this.type = options.type;
		this.speckID = options.speckID;

		// setup an in and out port
		/**
		 * Have again a problem with typing here.
		 * alignment params does exists based on source code,
		 * but typing says otherwise. Probably should use or extend DefaultPortModelOptions type for this.
		 */
		this.addPort(
			//@ts-ignore
			new DefaultPortModel({
				in: true,
				name: 'in',
				alignment: "center"
			})
		);
		this.addPort(
			//@ts-ignore
			new DefaultPortModel({
				in: false,
				name: 'out',
				alignment: "center"
			})
		);
	}

	public serialize() {
		return {
			...super.serialize(),
			color: this.color
		};
	}

	public deserialize(event: any): void {
		super.deserialize(event);
		this.color = event.data.color;
	}
}
