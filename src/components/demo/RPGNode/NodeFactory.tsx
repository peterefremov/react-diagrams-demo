import * as React from 'react';
import RPGNodeModel from './NodeModel';
import RPGNodeWidget from './NodeWidget';
import { AbstractReactFactory, GenerateWidgetEvent } from '@projectstorm/react-canvas-core';
import { DiagramEngine } from '@projectstorm/react-diagrams-core';

type Params = {
	onAddNew: () => void;
};

export default class NodeFactory extends AbstractReactFactory<RPGNodeModel, DiagramEngine> {

	private events: {
		onAddNew: () => void;
	};

	constructor(params: Params) {
		super('rpgnode');
		const {onAddNew} = params;

		this.events = {
			onAddNew
		};
	}

	//TODO: get a typing for this.
	public generateModel(initialConfig: any) {
		return new RPGNodeModel();
	}

	public generateReactWidget(event: GenerateWidgetEvent<any>): JSX.Element {
		const {onAddNew} = this.events;

		return <RPGNodeWidget onAddNew={onAddNew} engine={this.engine as DiagramEngine} node={event.model} />;
	}
}
